#include <stdio.h>

// ./argumentos_de_main Nombre
// ./argumentos_de_main Nombre espacio
// ./argumentos_de_main "Nombre espacio"

// *argv[] array de longitud indeterminada

// argc = cantidad de argumentos
// argv = acceder al argumento

// ./argumentos_de_main "Fer Mercado" "Otro argumento"
// ./argumentos_de_main Fer Otro

int main(int argc, char const *argv[]){		

	if(argc != 3){
		printf("Ha olvidado escribir su nombre (1er argumento)");
		printf(" Ó ha olvidado escribir otro argumento (2do argumento)\n");
		return 1;
	}	
	printf("Desde el programa -->%s, Hola -->%s, con otro argumento -->%s\n", argv[0], argv[1], argv[2]);		
	int i;
	for(i = 0; i < argc; i++){
		printf("argumento[%d] = %s\n", i, argv[i]);
	}
	return 0;
}